Web API with an endpoint at /products that will return products.

Query parameters available for filtering:
- maxPrice (decimal)
- minPrice (decimal)
- maxRating (decimal)
- minRating (decimal)
- isFavourite (bool)


Swagger endpoint also available

The solution is buildable and runnable when opened in Microsoft Visual Studio.

It can also be built and ran with the `dotnet run` command in the TheBestThings project directory.
