﻿using System.Text.Json;

namespace TheBestThings.Services.Tests;

public class ProductsServiceTests
{
    private const string dataFilePath = "MOCK_DATA.json";
    private ProductsService productsService = new(dataFilePath);

    [Fact]
    public void GetProductsTest()
    {
        FileStream? stream = File.OpenRead(dataFilePath);
        List<Product>? data = JsonSerializer.Deserialize<List<Product>>(stream) ?? throw new InvalidOperationException();

        Assert.Equal(data, productsService.GetProducts());
    }
}