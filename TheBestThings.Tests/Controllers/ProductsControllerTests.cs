﻿using Microsoft.Extensions.Logging;
using System.Text.Json;
using TheBestThings.Services;

namespace TheBestThings.Controllers.Tests;

public class ProductsControllerTests
{
    private readonly ProductsController _controller;

    private readonly List<Product> testProducts;

    public ProductsControllerTests()
    {
        // Retrieve data from JSON for testing
        FileStream? stream = File.OpenRead("MOCK_DATA.json");
        testProducts = JsonSerializer.Deserialize<List<Product>>(stream) ?? throw new InvalidOperationException();

        // Mock products service will return the dataset previously created
        Mock<IProductsService> mockProductsService = new();
        mockProductsService.Setup(x => x.GetProducts()).Returns(testProducts);

        _controller = new ProductsController(
            new Mock<ILogger<ProductsController>>().Object,
            mockProductsService.Object);
    }

    [Fact]
    public void GetTest()
    {
        Assert.Equal(testProducts, _controller.Get());
    }

    public static IEnumerable<object?[]> PriceRangeFilterData => new List<object?[]>
        {
            new object?[] { 0m, 600m },
            new object?[] { 300m, 900m },
            new object?[] { null, 700m },
            new object?[] { 200m, null}
        };

    [Theory]
    [MemberData(nameof(PriceRangeFilterData))]
    public void GetPriceRangeFilterTest(decimal? minPrice, decimal? maxPrice)
    {
        IEnumerable<Product> products = _controller.Get(minPrice: minPrice, maxPrice: maxPrice);

        Assert.All(products, product => Assert.InRange(product.Price, minPrice ?? decimal.MinValue, maxPrice ?? decimal.MaxValue));
    }

    public static IEnumerable<object?[]> RatingRangeFilterData => new List<object?[]>
        {
            new object?[] { 0m, 5m },
            new object?[] { 1.1m, 4.3m },
            new object?[] { null, 4.1m },
            new object?[] { 2.1m, null}
        };

    [Theory]
    [MemberData(nameof(RatingRangeFilterData))]
    public void GetRatingRangeFilterTest(decimal? minRating, decimal? maxRating)
    {
        IEnumerable<Product> products = _controller.Get(minRating: minRating, maxRating: maxRating);

        Assert.All(products, product => Assert.InRange(product.Attribute.Rating.Value,
            minRating ?? decimal.MinValue, maxRating ?? decimal.MaxValue));
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    [InlineData(null)]
    public void GetFantasticFilterTest(bool? isFantastic)
    {
        IEnumerable<Product> products = _controller.Get(isFantastic: isFantastic);

        if (isFantastic is null)
            Assert.Equal(products, testProducts);
        else if (isFantastic == true)
            Assert.All(products, product => Assert.True(product.Attribute.Fantastic.Value));
        else
            Assert.All(products, product => Assert.False(product.Attribute.Fantastic.Value));

    }
}