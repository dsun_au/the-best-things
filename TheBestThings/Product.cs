﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TheBestThings;

public record Fantastic
{
    [Required]
    [JsonPropertyName("name")]
    public string Name { get; set; } = string.Empty;

    [Required]
    [JsonPropertyName("type")]
    public int Type { get; set; }

    [Required]
    [JsonPropertyName("value")]
    public bool Value { get; set; }

    public Fantastic(string name, int type, bool value) =>
    (Name, Type, Value) = (name, type, value);
}

public record Rating
{
    [Required]
    [JsonPropertyName("name")]
    public string Name { get; set; } = string.Empty;

    [Required]
    [JsonPropertyName("type")]
    public string Type { get; set; } = string.Empty;

    [Required]
    [JsonPropertyName("value")]
    public decimal Value { get; set; }

    public Rating(string name, string type, decimal value) =>
        (Name, Type, Value) = (name, type, value);
}

public record Attribute
{
    [Required]
    [JsonPropertyName("fantastic")]
    public Fantastic Fantastic { get; set; }

    [Required]
    [JsonPropertyName("rating")]
    public Rating Rating { get; set; }

    public Attribute(Fantastic fantastic, Rating rating) =>
        (Fantastic, Rating) = (fantastic, rating);
}

public record Product
{
    [Required]
    [JsonPropertyName("id")]
    public int ID { get; }

    [Required]
    [JsonPropertyName("sku")]
    public string SKU { get; }

    [Required]
    [JsonPropertyName("name")]
    public string Name { get; }

    [Required]
    [JsonPropertyName("price")]
    public decimal Price { get; }

    [Required]
    [JsonPropertyName("attribute")]
    public Attribute Attribute { get; }

    public Product(int id, string sku, string name, decimal price, Attribute attribute) =>
        (ID, SKU, Name, Price, Attribute) = (id, sku, name, price, attribute);
}