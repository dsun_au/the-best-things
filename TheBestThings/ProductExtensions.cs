﻿namespace TheBestThings;

internal static class ProductExtensions
{
    public static void FilterByMaxPrice(this List<Product> products, decimal maxPrice)
    {
        products.RemoveAll(product => product.Price > maxPrice);
    }

    public static void FilterByMinPrice(this List<Product> products, decimal minPrice)
    {
        products.RemoveAll(product => product.Price < minPrice);
    }

    public static void FilterByMaxRating(this List<Product> products, decimal maxRating)
    {
        products.RemoveAll(product => product.Attribute.Rating.Value > maxRating);
    }

    public static void FilterByMinRating(this List<Product> products, decimal minRating)
    {
        products.RemoveAll(product => product.Attribute.Rating.Value < minRating);
    }

    public static void FilterByFantastic(this List<Product> products, bool isFantastic)
    {
        products.RemoveAll(product => product.Attribute.Fantastic.Value != isFantastic);
    }
}