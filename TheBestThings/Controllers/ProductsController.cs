﻿using TheBestThings.Services;

namespace TheBestThings.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IProductsService _productsService;

        public ProductsController(ILogger<ProductsController> logger, IProductsService productsService)
        {
            _logger = logger;
            _productsService = productsService;
        }

        [HttpGet]
        public IEnumerable<Product> Get(
            [FromQuery] decimal? maxPrice = null,
            [FromQuery] decimal? minPrice = null,
            [FromQuery] bool? isFantastic = null,
            [FromQuery] decimal? maxRating = null,
            [FromQuery] decimal? minRating = null)
        {
            List<Product> products = _productsService.GetProducts().ToList();

            if (maxPrice is not null)
                products.FilterByMaxPrice(maxPrice.Value);

            if(minPrice is not null)
                products.FilterByMinPrice(minPrice.Value);

            if(maxRating is not null)
                products.FilterByMaxRating(maxRating.Value);

            if(minRating is not null)
                products.FilterByMinRating(minRating.Value);

            if (isFantastic is not null)
                products.FilterByFantastic(isFantastic.Value);

            _logger.LogInformation("Returning {number} entries", products.Count);

            return products;
        }


    }
}
