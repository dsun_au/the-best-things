﻿namespace TheBestThings.Services;

public interface IProductsService
{
    IEnumerable<Product> GetProducts();
}