﻿using System.Text.Json;

namespace TheBestThings.Services
{
    public class ProductsService : IProductsService
    {
        protected string FilePath;

        private readonly List<Product>? _products;
        protected List<Product> Products => _products ?? JsonSerializer.Deserialize<List<Product>>(File.OpenRead(FilePath)) ?? new List<Product>();

        public ProductsService(string filePath)
        {
            FilePath = filePath;
        }

        public IEnumerable<Product> GetProducts()
        {
            return Products;
        }
    }
}
